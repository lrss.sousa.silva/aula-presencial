CREATE TABLE CUSTOMERS (
    codCli number(3),
    nomeCli varchar2(250) not null,
    email varchar2(225) not null,
    statusCad raw(1) not null,
    constraint pk1 primary key (codcli) 
);

CREATE TABLE ORDERS (
    orderId number(3),
    codCli number(3) not null, -- Customers 
    orderdate date not null,
    shipdate date not null,
    StatusOrder raw(1) not null,
    constraint pk2 primary key (orderId), 
    constraint fk1 foreign key(codCli) references CUSTOMERS (codCli) 
);

CREATE TABLE SUPPLIERS(
    CodSup number(3) not null,
    NomeSup varchar2(100) not null
    constraint pk3 primary key (CodSup) 
);

CREATE TABLE CATEGORIES(
    CodCat number(3) not null,
    CatName varchar2(200) not null,
    constraint pk4 primary key (CodCat) 
);

CREATE TABLE PRODUCTS (
    ProductId number(3),
    CodCat number(3) not null, -- Category
    CodSup number(3) not null, -- Supplier
    ProductName varchar(200) not null,
    Statusprod raw(1) not null,
    ValProd number(10,2) not null
    constraint fk2 foreign key(codCat) references CATEGORIES (codCat),
    constraint fk3 foreign key(CodSup) references CATEGORIES (CodSup) 
);

CREATE TABLE ORDERDETAILS (
    OrderId number(3), -- Order
    ProductId number(3) not null, -- Product
    QtdProd number(3) not null,
    ValProdVenda number(10,2) not null,
    constraint fk4 foreign key(orderId) references ORDERS (orderId), 
    constraint fk5 foreign key(ProductId) references CATEGORIES (ProductId) 
);

